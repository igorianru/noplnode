'use strict';

var express = require('express');
var app = express();
var http = require('http');
var port = process.env.PORT || 8010;
var ejsLocals = require('ejs-locals');
var bodyParser = require('body-parser');
var Primus = require('primus');
var Rooms = require('primus-rooms');
var redis = require("redis"),
    client_r = redis.createClient();
var db = require(__dirname + '/app/config/db');
var db_con = db.db();
app.db = db_con;
client_r.on("error", function (err) {
    console.log("Error " + err);
});

app.client_r = client_r;


var server = http.createServer(app);
var primus = new Primus(server,
    {
        transformer: 'sockjs',
        parser: 'JSON',
        methods: 'GET,HEAD,PUT,POST,DELETE,OPTIONS',
        credentials: true,
        maxAge: '30 days',
        exposed: true
    }
);

// add rooms to Primus
primus.use('rooms', Rooms);

app.primus = primus;
// configuration settings
app.engine('ejs', ejsLocals);
app.set('views', __dirname + '/app/views');
app.set('view engine', 'ejs');
app.set('models', __dirname + '/app/models');
//app.set('port', process.env.PORT || 8010);

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/node_modules'));

// set view locals
app.use(function (req, res, next) {
    app.locals.route = req.url;
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));

    res.header("Access-Control-Allow-Origin", "http://nopl.ru");
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-  Override, Content-Type, Accept');

    next()
});
// Routes


module.exports = app;

var routes = require('./app/routes')(app);
var routes2 = require('./app/modules/routes')(app);




server.listen(port, function(){
    console.log('Express server listening on port ' + port);
});