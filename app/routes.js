// mount routes
module.exports = function(app){
    // views
    var pages = require(__dirname + '/controllers/pages');
    var reviews = require(__dirname + '/controllers/reviews');
    var wall = require(__dirname + '/controllers/wall');

    callback();
    function callback() {
        app.use(function (req, res, next) {

            next(routes(req.headers.origin));
        });
    }

    function routes(url)
    {
        console.log(url);
        //if(url == "http://nopl.ru" || url == "https://nopl.ru" )
        //{
            app.post('/reviews/all/:table?/:id?/:id_max?', reviews.reviews_all);
            //app.get('/reviews/all/:table?/:id?/:id_max?', reviews.reviews_all);

            app.post('/reviews/reviews_add', reviews.reviews_add);
            //app.get('/reviews/reviews_add/:id?', reviews.reviews_add);

            app.post('/reviews/reviews_get_id/:table?/:id?', reviews.reviews_get_id);
            app.get('/reviews/reviews_get_id/:table?/:id?', reviews.reviews_get_id);


            app.post('/reviews/reviews_del', reviews.reviews_del);



        //////////////////////////////
        app.post('/wall/all/:table?/:id?/:id_max?', wall.wall_all);
        app.get('/wall/all/:table?/:id?/:id_max?', wall.wall_all);//

        app.post('/wall/wall_add', wall.wall_add);
        //app.get('/wall/wall_add/:id?', wall.wall_add);

        app.post('/wall/wall_get_id/:table?/:id?', wall.wall_get_id);
        app.get('/wall/wall_get_id/:table?/:id?', wall.wall_get_id);


        app.post('/wall/wall_del', wall.wall_del);


            //app.get('/', function (req, res) { res.redirect('home') });
            //app.get('/home', pages.home);
            //app.get('/about', pages.about);

        //} else {
            app.use(pages.error_404);
        //}
    }
};

