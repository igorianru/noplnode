//var wall_model = require(__dirname + '/../models/wall_model');
var app = require(__dirname + '/../../app.js');
var co = require(__dirname + "/../libraries/cookie.js");
exports.wall_all = function (req, res) {
    var cookieManager = new co.cookie(req.headers.cookie);
    var cook = cookieManager.cookies;
    var db = app.db;
    var r = req.params;
     //req['body']['max_id'] = 0;
    var sql = "SELECT " +
        " nopl_" + r.table + ".id ," +
        " nopl_" + r.table + ".id_object ," +
        " nopl_" + r.table + ".id_user ," +
        " nopl_" + r.table + ".text ," +
        " nopl_" + r.table + ".date_time ," +
        " nopl_" + r.table + ".date_time ," +
        " nopl_" + r.table + ".mass_img ," +
        " nopl_" + r.table + ".mass_file ," +

        " nopl_users.name ," +
        " nopl_users.surname ," +
        " nopl_users.img " +

        " FROM  nopl_" + r.table + " " +
        " INNER JOIN nopl_users " +
        " ON nopl_users.id = nopl_" + r.table + ".id_user " ;

    if(parseInt(req.body.max_id) == 0) {
        sql += " WHERE nopl_" + r.table + ".id_object='" + r.id + "' ORDER BY nopl_" + r.table + ".id ASC LIMIT 0, 10;";
    } else {
        var l_to =   parseInt(req.body.max_id) * 10 ;

        var l_from = 10;

        sql += " WHERE nopl_" + r.table + ".id_object='" + r.id + "' ORDER BY nopl_" + r.table + ".id ASC LIMIT "+l_to+", "+l_from+" ;";
    }
    //console.log(sql)
    db.query(sql, function (err, rows, fields) {
        if (err) throw err;

        var body = {
            'error': err,
            'result': res
        };
        view(body, rows);
    });

    function view(body, rows) {

        res.render('wall/all', {
            title: 'Wall'
            , rows: rows
            , id: ''
            , id_user: cook.id_user
            , max_id: req.body.max_id
        })
        //res.render('wall/all', {rows:  rows, id_user:  cook.id_user}, function(err, html) {
        //    body.html = html;
        //    res.send(JSON.stringify(body));
        //});
    }
};
// insert new wall
exports.wall_add = function (req, res) {

    var cookieManager = new co.cookie(req.headers.cookie);
    var cook = cookieManager.cookies;
    var r = req.body;
    var db = app.db;
    var date = new Date();
    var getMonth = date.getMonth() + 1;
    date = date.getFullYear() + '-' + getMonth + '-' + date.getUTCDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();

    if (cook.id_user == r.id_user) {
        var sql =
            "INSERT INTO nopl_" + r.table + " " +
            " (`id_object`, `id_user`, `date_time`, `text`) " +
            " VALUES " +
            " ('" + r.id_object + "', '" + cook.id_user + "', '" + date + "', '" + r.text + "') ;";

        db.query(sql, function (err, res, fields) {
            if (err) throw err;

            //var body ={
            //    'error':err,
            //    'result':res
            //};
            get_id(res.insertId, '');

        });
    } else {
        get_id(0, 'error');
    }


    function get_id(id, e) {
        if (e != 'error') {
            var table = 'nopl_' + r.table;
            var sql = "SELECT " +
                " " + table + ".id ," +
                " " + table + ".id_object ," +
                " " + table + ".id_user ," +
                " " + table + ".date_time ," +
                " " + table + ".text ," +
                " " + table + ".mass_img ," +
                " " + table + ".mass_file ," +

                " nopl_users.name ," +
                " nopl_users.surname ," +
                " nopl_users.img " +

                " FROM " + table + " " +
                " INNER JOIN nopl_users " +
                " ON nopl_users.id = " + table + ".id_user " +


                " WHERE `" + table + "`.`id`='" + id + "' ;";
//console.log(sql)
            db.query(sql, function (err, rows, fields) {
                if (err) throw err;

                var body = {
                    'error': err,
                    'result': rows
                };

                view(rows, body);
            });
        } else {
            view('', e);
        }


        function view(rows, body) {
            res.render('wall/all', {rows: rows, id_user: cookieManager.cookies.id_user}, function (err, html) {
                /*                console.log(err)
                 console.log(html)*/
                body.html = html;
                res.send(JSON.stringify(body));
            });
        }
    }
};
// insert get id wall
exports.wall_get_id = function (req, res) {
    function htmlspecialchars(string, quote_style, charset, double_encode) {
        var optTemp = 0,
            i = 0,
            noquotes = false;
        if (typeof quote_style === 'undefined' || quote_style === null) {
            quote_style = 2;
        }
        string = string.toString();
        if (double_encode !== false) { // Put this first to avoid double-encoding
            string = string.replace(/&/g, '&amp;');
        }
        string = string.replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');

        var OPTS = {
            'ENT_NOQUOTES': 0,
            'ENT_HTML_QUOTE_SINGLE': 1,
            'ENT_HTML_QUOTE_DOUBLE': 2,
            'ENT_COMPAT': 2,
            'ENT_QUOTES': 3,
            'ENT_IGNORE': 4
        };
        if (quote_style === 0) {
            noquotes = true;
        }
        if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
            quote_style = [].concat(quote_style);
            for (i = 0; i < quote_style.length; i++) {
                // Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
                if (OPTS[quote_style[i]] === 0) {
                    noquotes = true;
                } else if (OPTS[quote_style[i]]) {
                    optTemp = optTemp | OPTS[quote_style[i]];
                }
            }
            quote_style = optTemp;
        }
        if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
            string = string.replace(/'/g, '&#039;');
        }
        if (!noquotes) {
            string = string.replace(/"/g, '&quot;');
        }

        return string;
    }
    var sql;
    var cookieManager = new co.cookie(req.headers.cookie);
    var cook = cookieManager.cookies;
    var db = app.db;
    var b = req.body;
    var table = 'nopl_' + b.table;
    var metod = b.metod;

    if(metod == 'get') {
        sql = "SELECT " +
            " " + table + ".id ," +
            " " + table + ".id_object ," +
            " " + table + ".id_user ," +
            " " + table + ".date_time ," +
            " " + table + ".text ," +
            " " + table + ".mass_img ," +
            " " + table + ".mass_file ," +

            " nopl_users.name ," +
            " nopl_users.surname ," +
            " nopl_users.img " +

            " FROM  " + table + " " +
            " INNER JOIN nopl_users " +
            " ON nopl_users.id = " + table + ".id_user " +


            " WHERE `" + table + "`.`id`='" + b.id_wall + "' " +
            "AND  `" + table + "`.`id_user`='" + cook.id_user + "'  ;";

        db.query(sql, function (err, rows, fields) {
            if (err) throw err;

            var body = {
                'error': err,
                'result': rows
            };
            view(body, rows, 'edit');
        });
    } else {
        console.log(b.text);
        var text = b.text;
        sql = "UPDATE " + table + " " +
            " SET `text`='" + htmlspecialchars(text, 1) + "' " +
            " WHERE `id`='" + b.id_wall + "' " +
            "AND `id_user`='" + cook.id_user + "' ;";

        db.query(sql, function (err, rows, fields) {
            if (err) throw err;
            console.log(b);
            console.log(rows);
            sql = "SELECT " +
            " " + table + ".id ," +
            " " + table + ".id_object ," +
            " " + table + ".id_user ," +
            " " + table + ".date_time ," +
            " " + table + ".text ," +
            " " + table + ".mass_img ," +
            " " + table + ".mass_file ," +

            " nopl_users.name ," +
            " nopl_users.surname ," +
            " nopl_users.img " +

            " FROM  " + table + " " +
            " INNER JOIN nopl_users " +
            " ON nopl_users.id = " + table + ".id_user " +
            " WHERE `" + table + "`.`id`='" + b.id_wall + "' " +
            "AND  `" + table + "`.`id_user`='" + cook.id_user + "'  ;";

            db.query(sql, function (err, rows, fields) {
                if (err) throw err;

                var body = {
                    'error': err,
                    'result': rows
                };
                view(body, rows, 'edit_viv');
            });
        });
    }

    function view(body, rows, viw) {
        res.render('wall/'+viw, {rows: rows, id_user: cook.id_user}, function (err, html) {

            body.html = html;
            res.send(JSON.stringify(body));
        });
    }
};
exports.wall_del = function (req, res) {
    var cookieManager = new co.cookie(req.headers.cookie);
    var cook = cookieManager.cookies;
    var db = app.db;
    var b = req.body;
    var table = 'nopl_' + b.table;

    var sql = "DELETE FROM " + table + "  WHERE `id`='" + b.id_wall + "' AND `id_user`='" + cook.id_user + "' ;";
   console.log(sql)
    db.query(sql, function (err, rows, fields) {
        if (err) throw err;

        var body = {
            'error': err,
            'result': rows
        };
        res.send(JSON.stringify(body));
    });
};
