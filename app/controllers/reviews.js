//var reviews_model = require(__dirname + '/../models/reviews_model');
var app = require(__dirname + '/../../app.js');
var co = require(__dirname + "/../libraries/cookie.js");
exports.reviews_all = function (req, res) {
    var cookieManager = new co.cookie(req.headers.cookie);
    var cook = cookieManager.cookies;
    var db = app.db;
//console.log(db)
    var sql = "SELECT " +
        " nopl_rec_" + req.params.table + ".id ," +
        " nopl_rec_" + req.params.table + ".id_object ," +
        " nopl_rec_" + req.params.table + ".id_user ," +
        " nopl_rec_" + req.params.table + ".date_time ," +
        " nopl_rec_" + req.params.table + ".date_time ," +
        " nopl_rec_" + req.params.table + ".text ," +
        " nopl_rec_" + req.params.table + ".belongs ," +

        " nopl_users.name ," +
        " nopl_users.surname ," +
        " nopl_users.img " +

        " FROM  nopl_rec_" + req.params.table + " " +
        " INNER JOIN nopl_users " +
        " ON nopl_users.id = nopl_rec_" + req.params.table + ".id_user " ;

    if(parseInt(req.body.max_id) == 0) {
        sql += " WHERE id_object='" + req.params.id + "' ORDER BY `id`DESC LIMIT 0, 10;";
    } else {
        var l_to =   parseInt(req.body.max_id) * 10 ;
        var l_from = 10;

        sql += " WHERE id_object='" + req.params.id + "' ORDER BY `id` DESC LIMIT "+l_to+", "+l_from+" ;";
    }
    //console.log(sql)
    db.query(sql, function (err, rows, fields) {
        if (err) throw err;

        var body = {
            'error': err,
            'result': res
        };
        view(body, rows);
    });

    function view(body, rows) {

        res.render('reviews/all', {
            title: 'Reviews'
            , rows: rows
            , id: ''
            , id_user: cook.id_user
            , max_id: req.body.max_id
        })
        //res.render('reviews/all', {rows:  rows, id_user:  cook.id_user}, function(err, html) {
        //    body.html = html;
        //    res.send(JSON.stringify(body));
        //});
    }
};
// insert new review
exports.reviews_add = function (req, res) {

    var cookieManager = new co.cookie(req.headers.cookie);
    var cook = cookieManager.cookies;
    var r = req.body;
    var db = app.db;
    var date = new Date();
    var getMonth = date.getMonth() + 1;
    date = date.getFullYear() + '-' + getMonth + '-' + date.getUTCDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();

    if (cook.id_user == r.id_user) {
        var sql =
            "INSERT INTO nopl_rec_" + r.table + " " +
            " (`id_object`, `id_user`, `date_time`, `text`) " +
            " VALUES " +
            " ('" + r.id_object + "', '" + cook.id_user + "', '" + date + "', '" + r.text + "') ;";

        db.query(sql, function (err, res, fields) {
            if (err) throw err;

            //var body ={
            //    'error':err,
            //    'result':res
            //};
            get_id(res.insertId, '');

        });
    } else {
        get_id(0, 'error');
    }


    function get_id(id, e) {
        if (e != 'error') {
            var table = 'nopl_rec_' + r.table;
            var sql = "SELECT " +
                " " + table + ".id ," +
                " " + table + ".id_object ," +
                " " + table + ".id_user ," +
                " " + table + ".date_time ," +
                " " + table + ".date_time ," +
                " " + table + ".text ," +
                " " + table + ".belongs ," +

                " nopl_users.name ," +
                " nopl_users.surname ," +
                " nopl_users.img " +

                " FROM " + table + " " +
                " INNER JOIN nopl_users " +
                " ON nopl_users.id = " + table + ".id_user " +


                " WHERE `" + table + "`.`id`='" + id + "' ;";
//console.log(sql)
            db.query(sql, function (err, rows, fields) {
                if (err) throw err;

                var body = {
                    'error': err,
                    'result': rows
                };

                view(rows, body);
            });
        } else {
            view('', e);
        }


        function view(rows, body) {
            res.render('reviews/all', {rows: rows, id_user: cookieManager.cookies.id_user}, function (err, html) {
                /*                console.log(err)
                 console.log(html)*/
                body.html = html;
                res.send(JSON.stringify(body));
            });
        }
    }
};
// insert get id review
exports.reviews_get_id = function (req, res) {
    function htmlspecialchars(string, quote_style, charset, double_encode) {
        var optTemp = 0,
            i = 0,
            noquotes = false;
        if (typeof quote_style === 'undefined' || quote_style === null) {
            quote_style = 2;
        }
        string = string.toString();
        if (double_encode !== false) { // Put this first to avoid double-encoding
            string = string.replace(/&/g, '&amp;');
        }
        string = string.replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');

        var OPTS = {
            'ENT_NOQUOTES': 0,
            'ENT_HTML_QUOTE_SINGLE': 1,
            'ENT_HTML_QUOTE_DOUBLE': 2,
            'ENT_COMPAT': 2,
            'ENT_QUOTES': 3,
            'ENT_IGNORE': 4
        };
        if (quote_style === 0) {
            noquotes = true;
        }
        if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
            quote_style = [].concat(quote_style);
            for (i = 0; i < quote_style.length; i++) {
                // Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
                if (OPTS[quote_style[i]] === 0) {
                    noquotes = true;
                } else if (OPTS[quote_style[i]]) {
                    optTemp = optTemp | OPTS[quote_style[i]];
                }
            }
            quote_style = optTemp;
        }
        if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
            string = string.replace(/'/g, '&#039;');
        }
        if (!noquotes) {
            string = string.replace(/"/g, '&quot;');
        }

        return string;
    }
    var sql;
    var cookieManager = new co.cookie(req.headers.cookie);
    var cook = cookieManager.cookies;
    var db = app.db;
    var b = req.body;
    var table = 'nopl_rec_' + b.table;
    var metod = b.metod;

    if(metod == 'get') {
        sql = "SELECT " +
            " " + table + ".id ," +
            " " + table + ".id_object ," +
            " " + table + ".id_user ," +
            " " + table + ".date_time ," +
            " " + table + ".text ," +
            " " + table + ".belongs ," +

            " nopl_users.name ," +
            " nopl_users.surname ," +
            " nopl_users.img " +

            " FROM  " + table + " " +
            " INNER JOIN nopl_users " +
            " ON nopl_users.id = " + table + ".id_user " +


            " WHERE `" + table + "`.`id`='" + b.id_rev + "' " +
            "AND  `" + table + "`.`id_user`='" + cook.id_user + "'  ;";

        db.query(sql, function (err, rows, fields) {
            if (err) throw err;

            var body = {
                'error': err,
                'result': rows
            };
            view(body, rows, 'edit');
        });
    } else {
        console.log(b.text);
        var text = b.text;
        sql = "UPDATE " + table + " " +
            " SET `text`='" + htmlspecialchars(text, 1) + "' " +
            " WHERE `id`='" + b.id_rev + "' " +
            "AND `id_user`='" + cook.id_user + "' ;";

        db.query(sql, function (err, rows, fields) {
            if (err) throw err;
            console.log(b);
            console.log(rows);
            sql = "SELECT " +
            " " + table + ".id ," +
            " " + table + ".id_object ," +
            " " + table + ".id_user ," +
            " " + table + ".date_time ," +
            " " + table + ".text ," +
            " " + table + ".belongs ," +

            " nopl_users.name ," +
            " nopl_users.surname ," +
            " nopl_users.img " +

            " FROM  " + table + " " +
            " INNER JOIN nopl_users " +
            " ON nopl_users.id = " + table + ".id_user " +
            " WHERE `" + table + "`.`id`='" + b.id_rev + "' " +
            "AND  `" + table + "`.`id_user`='" + cook.id_user + "'  ;";

            db.query(sql, function (err, rows, fields) {
                if (err) throw err;

                var body = {
                    'error': err,
                    'result': rows
                };
                view(body, rows, 'edit_viv');
            });
        });
    }

    function view(body, rows, viw) {
        res.render('reviews/'+viw, {rows: rows, id_user: cook.id_user}, function (err, html) {

            body.html = html;
            res.send(JSON.stringify(body));
        });
    }
};
exports.reviews_del = function (req, res) {
    var cookieManager = new co.cookie(req.headers.cookie);
    var cook = cookieManager.cookies;
    var db = app.db;
    var b = req.body;
    var table = 'nopl_rec_' + b.table;

    var sql = "DELETE FROM " + table + "  WHERE `id`='" + b.id_rev + "' AND `id_user`='" + cook.id_user + "' ;";
   console.log(sql)
    db.query(sql, function (err, rows, fields) {
        if (err) throw err;

        var body = {
            'error': err,
            'result': rows
        };
        res.send(JSON.stringify(body));
    });
};
