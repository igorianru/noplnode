exports.home = function (req, res) {
    res.render('pages/home', {
        title: 'Home page'
        , message: 'This is the "home" action of "pages" controller'
    })
};
exports.about = function (req, res) {
    res.render('pages/about', {
        title: 'about page'
        , message: 'This is the "about" action of "pages" controller'
    })
};
// error 404
exports.error_404 = function (req, res) {
    res.render('pages/error_404', {
        title: 'Error 404'
        , message: 'Error 404'
    })
}