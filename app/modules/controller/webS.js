// проверка пользователя на онлайн
exports.get_user_online = function (ws, param) {

    var client_r = ws.client_r;
    var spark = ws.spark;
    var message;

    try {
        client_r.get(param.id + "_user", function (err, reply) {
            if (reply != null) {
                message = {
                    global: 'user',
                    body: {method: 'get_user_online', param: {user: param.id, time: reply}}
                };
            } else {
                message = {
                    global: 'user',
                    body: {method: 'get_user_online', param: {user: param.id, time: "0"}}
                };
            }
            spark.write(message);
            console.log(message);
        });
    } catch (e) {
        console.log(e);
    }
};

// добавление пользователя на онлайн
exports.set_user_online = function (ws, param) {
    try {
        var client_r = ws.client_r;
        var spark = ws.spark;
        var message;

        var dateObj = new Date();
        var set_r = client_r.set(param.id + "_user", dateObj.getTime());
        var expire = client_r.expire(param.id + "_user", 600);

        message = {global: 'user', body: {method: 'set_user_online', param: {set: set_r, expire: expire}}};

        spark.write(message);
        console.log(message);
    } catch (e) {
        console.log(e);
    }
};



// отправка сообщения
exports.enter_mess = function (ws, param, room, text) {
    try {

        var spark = ws.spark;
        var message;

        // check if spark is already in this room
        if (~spark.rooms().indexOf(room)) {
            send();
        } else {
            // join the room
            spark.join(room, function(){
                send();
            });
        }

        // send to all clients in the room
        function send() {
            message = {
                global: 'messages', body:
                {
                    method: 'enter_mess',
                    param:
                    {
                        text: text,
                        mess_id:0,
                        id_user: param.id_user,
                        id_dialog: param.id_dialog
                    }
                }
            };
            spark.room(room).write(message);
            console.log(message)
        }




    } catch (e) {
        console.log(e);
    }
};




























// mount webS
exports.webS = function (ws) {


    var data = ws.data;
    data = data || {};
    var action = data.action;
    var room = data.room;
    var msg = data.msg;

    // join a room
    if ('join' === action) {
        spark.join(room, function () {

            // send message to this client
            spark.write('you joined room ' + room);

            // send message to all clients except this one
            spark.room(room).except(spark.id).write(spark.id + ' joined room ' + room);
        });
    }

    // leave a room
    if ('leave' === action) {
        spark.leave(room, function () {

            // send message to this client
            spark.write('you left room ' + room);
        });
    }

    // chat a room
    if ('msg' === action) {
        spark.leave(room, function () {

            // send message to this client
            spark.room(room).except(spark.id).write(msg);
//                spark.write(msg);
        });
    }

// проверка авторизации
    function _check_login() {
        try {

            connect_mysql.query('SELECT id FROM  nopl_users ', function (err, rows, fields) {
                if (err) throw err;

                //conn.write('The solution is: '+rows[1]['id']);
                //console.log(rows);

            });

        } catch (e) {
            console.log(e);
        }
    }

    //var fff = app.use;
    /*   function gggg(j)
     {

     return fff;
     }*/
// достаём из базы комментарии
    function get_reviews(param) {
        try {
            //'http://nopl.ru/user/login_node',
            //request.get(
            //
            //    'http://socks.nopl.ru/',
            //    { form: { key_secret: 'uhi#9vvd3Fg6we' } },
            //    function (error, response, body) {
            //        var cookieManager = new co.cookie(response.headers.cookie);
            //        console.log(cookieManager.cookies);
            //    }
            //)

            connect_mysql.query('SELECT id FROM  nopl_users ', function (err, rows, fields) {
                if (err) throw err;

                //conn.write('The solution is: '+rows[1]['id']);
                //console.log(rows);

                var res = '';
                for (var i = 0; i < rows.length; ++i) {
                    //sys.puts("Result: "+sys.inspect(result.toHash(result.records[i])));

                    res += '<div class="row">' +
                        '<div class="col-md-1">' +
                        '<a class="media-left" href="/id1">' +
                        '<div style="width: 64px; height: 64px; background: #c5c5c6">' +
                        '<img src="/images/profile/id1/crop/n530478z698836n511929t459417m55522.jpg" style="width: 64px;height: 64px;"/> ' +
                        '</div>' +
                        '</a>' +
                        '</div>' +
                        '<div class="col-md-10">' +
                        '<div style="text-align: left; width: 100%">' +
                        '<a href="/id1">Игорь Носачёв</a>' +
                        '2014-11-25' +
                        '</div>' +
                        '<div class="media-body"> hfghgfhgfhgf </div>' +
                        '</div>' +
                        '</div>';

                    if (rows.length - 1 > i) {
                        res += '<hr />';
                    }


                }


                message = {global: 'reviews', body: {method: 'get_reviews', param: {result: '', html: res}}};
                spark.write(message);

            });

            /*  connect_mysql.query("SELECT * FROM nopl_users;",
             function(result) {
             for(var i=0; i<result.records.length; ++i) {
             //sys.puts("Result: "+sys.inspect(result.toHash(result.records[i])));

             console.log(sys.inspect(result.toHash(result.records[i])));
             };
             },
             function(error) {
             //sys.puts("Error: "+sys.inspect(error));
             });
             */

            //console.log(rows[1]['id']);
        } catch (e) {
            console.log(e);
        }
    }


}