// mount routes
module.exports = function (app) {

    var primus = app.primus;
    var ws = {};

    primus.on('connection', function connection(spark) {
        spark.on('data', function received(data, res_t) {

            data = data || {};
            //var action = data.action;
            var room = data.body.param.room;
            var method = data.body.method;
            var param = data.body.param;
            var text = data.body.param.text;

            ws.data = data;
            ws.res_t = res_t;
            ws.client_r = app.client_r;
            ws.connect_mysql = app.db;
            ws.spark = spark;
            var webS = require(__dirname + '/controller/webS');

            // router
            // раздел user
            if (data.global == 'user') {
                // проверка пользователя на онлайн
                if (method == 'get_user_online') {
                    webS.get_user_online(ws, param);
                }
                // добавление пользователя на онлайн
                if (method == 'set_user_online') {
                    webS.set_user_online(ws, param);
                }
                // запрос комментариев
                if (method == 'get_reviews') {
                    webS.get_reviews(param);
                }
            }

            // раздел messages
            if (data.global == 'messages') {
                // отправка сообщения
                if (method == 'enter_mess') {
                    webS.enter_mess(ws, param, room, text);
                    console.log(param)

                }



                // join a room
                if ('join' == method) {

                    spark.join(room, function () {

                        // send message to this client
                        spark.write('you joined room ' + room);

                        // send message to all clients except this one
                        var mass = spark.id + ' joined room ' + room;

                        spark.room(room).except(spark.id).write(mass);
                        console.log(mass)
                    });


                }

                // leave a room
                if ('leave' === method) {
                    spark.leave(room, function () {

                        // send message to this client
                        spark.write('you left room ' + room);
                    });
                }
            }
        });
    });
};